import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root',
})
export class ApiService {

  constructor(
    private http: HttpClient
  ) { }

  api = "http://localhost:4040"

  getProducts() {
    const url = `${this.api}/getProducts`
    return this.http.get(url);
  }

  getPromotions(date: Date) {
    const url = `${this.api}/getPromotions`
    const dateString = `${date.getFullYear()}-${(date.getMonth() + 1 + "").padStart(2, '0')}-${(date.getDate() + '').padStart(2, '0')}`
    return this.http.get(url, {
      params: {
        date: dateString
      }
    });
  }

  getReccom(products: number[]) {
    const url = `${this.api}/postRecommendation`
    return this.http.post(url, { productList: products }, { headers: { 'Content-Type': 'application/json' } });
  }

  getTotal(products: number[]) {
    const url = `${this.api}/postTotal`
    return this.http.post(url, { productList: products }, { headers: { 'Content-Type': 'application/json' } });
  }

}
