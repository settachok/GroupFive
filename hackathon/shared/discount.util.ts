export type Product = { id: number, name: string, price: number };
export type Promotion = { id: number, promo_name: string, products: number[], price: number };
export type Discount = { promotion: number, count: number, total: number };

export function checkPromotionByProductId(promotions: Promotion[], product_id: number) {
    console.log(JSON.stringify({promotions, product_id}))
    const tempPro: Promotion[] = []
    for (const promotion of promotions) {
        if (promotion.products.includes(product_id)) {
            tempPro.push(promotion)
        }
    }
    return tempPro;
}

export function getUnique(array: any[]) {
    return [...new Set(array)];
}

export function sortByKey<T>(array: T[], key: string, type: 'asc' | 'desc' = 'asc') {
    return array.sort((a: any, b: any) => (a[key] > b[key] ? 1 : -1) * (type == 'desc' ? -1 : 1))
}

export function findByKey<T>(array: T[], key: string, value: any) {
    return array.filter((e: any) => e[key] == value)
}

export function hasSubArray(master: number[], sub: number[]) {
    master = master.sort()
    sub = sub.sort()
    return sub.every(((i: number) => (v: number) => i = master.indexOf(v, i) + 1)(0));
}

export function countSubArray(master: number[], sub: number[]) {
    const tempMaster = [...master];
    let result = true;
    let count = 0;
    while (result) {
        result = hasSubArray(tempMaster, sub)
        if (result) {
            count++;
            sub.forEach((n) => {
                tempMaster.splice(tempMaster.indexOf(n), 1)
            })
        }
    }
    return count
}

export function getDiscountList(promotions: Promotion[], cart: number[]) {
    const discountList: Discount[] = promotions.map(promotion => {
        const countPromotionInCart = countSubArray(cart, promotion.products);
        const total = countPromotionInCart * promotion.price
        return {
            promotion: promotion.id,
            count: countPromotionInCart,
            total: total
        }
    })
    return discountList
}

export function calculateDiscount(promotions: Promotion[], cart: number[], promolog: number[] = [], forceIndex?: number) {
    // console.log('cart:',cart)
    const tempCart = [...cart];
    let tempLog = [...promolog];
    let totalSum = 0;
    while (tempCart.length > 0) {
        const discountList = getDiscountList(promotions, tempCart);
        const sortedDiscountList = sortByKey(discountList, 'total', "desc");
        if (sortedDiscountList.length == 0) break;
        const samePricedDiscountList = findByKey(sortedDiscountList, 'total', sortedDiscountList[0].total);
        // console.log({samePricedDiscountList, sortedDiscountList})
        if (samePricedDiscountList.length > 1 && forceIndex == undefined) {
            // console.log({cart: JSON.stringify({tempCart})})
            const tempSum = []
            for (let i = 0; i < samePricedDiscountList.length; i++) {
                tempSum.push(calculateDiscount(promotions, tempCart, tempLog, i))
            }
            tempCart.splice(0)
            // console.log({tempSum: JSON.stringify(tempSum)})
            const maxDiscountSum = sortByKey(tempSum, 'totalSum', 'desc');
            totalSum += maxDiscountSum[0].totalSum
            tempLog = maxDiscountSum[0].promolog
        }
        else {
            // console.log({sorteddc: JSON.stringify({tempLog, forceIndex, sorted: sortedDiscountList.map(e => e.promotion)})})
            const promotion = promotions.filter(e => e.id == sortedDiscountList[forceIndex ?? 0].promotion)[0];
            if (!hasSubArray(tempCart, promotion.products)) break;
            forceIndex = undefined;
            totalSum += promotion.price;
            tempLog.push(promotion.id)
            for (const product of promotion.products) {
                // console.log({product, tempCart});
                tempCart.splice(tempCart.indexOf(product), 1);
            }
        }
    }
    return { totalSum, promolog: tempLog };
}