import { Response } from 'express'
import { ResponseErrorTemplateInterface, ResponseInterface } from './interface/response.interface'

type errorTemplateList =
    "invalidParams"
    | "database"
    | "unknown"

export const responseErrorTemplateList: { [key in errorTemplateList]: ResponseErrorTemplateInterface } = {
    invalidParams: {
        HTTPStatus: 400,
        responseStatus: -1,
        message: "Error!, Invalid Input."
    },
    database: {
        HTTPStatus: 500,
        responseStatus: -1,
        message: "Error!, Internal Database Server Error."
    },
    unknown: {
        HTTPStatus: 500,
        responseStatus: -1,
        message: "Error!, Unknown Error. Please Contact Administrator"
    }
}

export const responseErrorTemplate = (res: Response, template: errorTemplateList) => {
    const err = responseErrorTemplateList[template];
    return responseError(res, err.message, err.responseStatus, err.HTTPStatus);
}

export const responseError = (res: Response, message: string = responseErrorTemplateList.unknown.message, responseStatus: number = -1, HTTPStatus: number = 400) => {
    const responseData: ResponseInterface = {
        status: responseStatus,
        message: message
    };
    return res.status(HTTPStatus).json(responseData);
}

export const responseOK = (res: Response, data?: any) => {
    const responseData: ResponseInterface = {
        status: 0,
        message: "OK",
        data: data
    };
    return res.status(200).json(responseData);
}
