import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  productList: any[] = [];
  cart: any[] = [];
  promotionList: any[] = [];
  totalDiscount = 0;
  promoLog: any[] = [];
  reccom: any[] = [];
  additionalReccom: any[] = [];

  constructor(
    private api: ApiService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.api.getProducts().subscribe({
      next: (res: any) => {
        this.productList = res.data
      }
    })

    this.api.getPromotions(new Date()).subscribe({
      next: (res: any) => {
        this.promotionList = res.data
      }
    })
  }

  addToCart(data: any) {
    this.cart.push(data);
  }

  removeFromCart(index: number) {
    this.cart.splice(index, 1)
  }

  getTotal() {
    return [...[0], ...this.cart.map(e => e.price)].reduce((p,c) => (+p) + (+c))
  }

  getPromoTotal() {
    return [...[0], ...this.promoLog.map(e => e.price)].reduce((p,c) => (+p) + (+c))
  }

  checkBill() {
    this.api.getTotal(this.cart.map(e => e.id)).subscribe({
      next: (res: any) => {
        console.log({ res })

        this.totalDiscount = res.data.totalSum;
        console.log(this.totalDiscount)
        this.promoLog = (res.data.promolog as any[]).map(e => {
          const discount = {...this.promotionList.filter(ee => ee.id == e)[0]};
          console.log({discount})
          discount.products = discount.products.split(',').map((ee: any) => this.productList.filter(eee => eee.id == ee)[0].name)
          return discount
        })
      }
    })

    this.api.getReccom(this.cart.map(e => e.id)).subscribe({
      next: (res: any) => {
        console.log({ res })

        this.reccom = res.data
      }
    })
  }

  getreccom(value: any) {
    console.log({ value })
    this.api.getReccom(value.value.split(',').map((e: any) => (+e))).subscribe({
      next: (res: any) => {
        console.log({ res })

        this.additionalReccom = res.data
      }
    })
  }

}
