export type ResponseInterface = {
    status: number;
    message: string;
    data?: any | Object;
}

export type ResponseErrorTemplateInterface = {
    responseStatus: number;
    HTTPStatus: number;
    message: string;
    data?: any | Object;
}