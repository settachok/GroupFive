export interface PromotionResponse {
    "id": number,
    "promo_name": string,
    "start_date": string,
    "end_date": string,
    "price": number,
    "products": number[],
}