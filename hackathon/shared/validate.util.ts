import { validate, Schema } from 'jsonschema'

type validateSchemaTypes = "string" | "number" | "object" | "array"



export type ValidateSchemaType<T> = {
    id?: string;
    type: validateSchemaTypes,
    properties: { [key in keyof T]: ValidateSchema | { type: validateSchemaTypes } },
    required: (keyof T)[]
}

export type ValidateSchema = {
    id?: string;
    type: validateSchemaTypes,
    properties: { [key: string]: ValidateSchema | { type: validateSchemaTypes } },
    required: string[]
}

export const validateJson = (object: any, schema: ValidateSchemaType<any>) => {
    return validate(object, schema)
}
