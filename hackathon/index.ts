import { getAllPromotion, getProducts, getPromotionByDate } from './api.service';
import { config as dotenvConfig } from 'dotenv';
import cors from 'cors'
import express from 'express';
import { responseError, responseOK } from './shared/response.util';
import { PromotionResponse } from './api.interface';
import { calculateDiscount, checkPromotionByProductId, getUnique } from './shared/discount.util'
dotenvConfig()

const app = express();
app.use(express.json());
app.use(cors())


app.get('/getPromotions', async (req, res) => {
    try {
        const promotions = await getPromotionByDate(req.query.date as string)
        return responseOK(res, promotions);
    }
    catch (e: any) {
        return responseError(res, e)
    }
})

app.get('/getProducts', async (req, res) => {
    try {
        const products = await getProducts()
        return responseOK(res, products);
    }
    catch (e: any) {
        return responseError(res, e)
    }
})

app.post('/postRecommendation', async (req, res) => {
    try {
        const productList: number[] = req.body.productList;
        const promotions = await getAllPromotion() as PromotionResponse[];
        // console.log({promotions})
        const promotion = (promotions).map((e) => {
            e.products = (e.products as any).trim().split(',').map((e: any) => parseInt(e))
            e.price = (+e.price)
            return e
        })
        console.log(promotion)

        const reccomList = []
        for (let i of productList) {
            const reccom = checkPromotionByProductId(promotion, i);
            reccomList.push(...reccom)
        }
        return responseOK(res, getUnique(reccomList));
    }
    catch (e: any) {
        return responseError(res, e)
    }
})

app.post('/postTotal', async (req, res) => {
    try {
        const productList: number[] = req.body.productList;
        const promotions = await getAllPromotion() as PromotionResponse[];
        // console.log({promotions})
        const promotion = (promotions).map((e) => {
            e.products = (e.products as any).trim().split(',').map((e: any) => parseInt(e))
            e.price = (+e.price)
            return e
        })
        // console.log(promotion, productList)

        // console.log({productList})

        const response = calculateDiscount(promotion, productList)
        return responseOK(res,response);
    }
    catch (e: any) {
        return responseError(res, e)
    }
})

app.listen(process.env.port, () => {
    console.log(`Listening on port: ${process.env.port}`);
});

