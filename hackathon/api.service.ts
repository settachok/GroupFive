import { QueryError } from 'mysql2';
import { pool, handleSqlError } from './shared/sql.util';

export const getPromotionByDate = async (date: string) => {
    try {
        const sql = `
            select p.*, group_concat(pd.product_id) as products from minihacka.promotion p
            left join minihacka.promotion_product pd on p.id = pd.promotion_id
            left join minihacka.product prod on pd.product_id = prod.id
            where :date BETWEEN p.start_date  and p.end_date 
            group by p.id
        `;
        const result = await pool.execute(sql, { date });
        return result[0];
    }
    catch (e: any) {
        throw handleSqlError(e)
    }
}

export const getAllPromotion = async () => {
    try {
        const sql = `
            select p.*, group_concat(pd.product_id) as products from minihacka.promotion p
            left join minihacka.promotion_product pd on p.id = pd.promotion_id
            left join minihacka.product prod on pd.product_id = prod.id
            group by p.id
        `;
        const result = await pool.execute(sql);
        return result[0];
    }
    catch (e: any) {
        throw handleSqlError(e)
    }
}

export const getProducts = async () => {
    try {
        const sql = `select * from minihacka.product`;
        const result = await pool.execute(sql);
        return result[0];
    }
    catch (e: any) {
        throw handleSqlError(e)
    }
}

//